require 'sidekiq/web'

Rails.application.routes.draw do
  root 'home#index'

  devise_scope :user do
    # Redirests signing out users back to sign-in
    get "users", to: "devise/sessions#new"
  end
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions' }

  authenticate :user, ->(user) { user.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  get 'wiki', to: 'home#wiki'
end
