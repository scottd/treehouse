require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of (:email) }
    it { should validate_presence_of (:first_name) }
    it { should validate_presence_of (:last_name) }

    describe '#has_eligibility' do
      let(:email) { 'test@example.com' }
      let(:user) { build(:user, email: email) }

      context 'when a related Eligible instance does not exist' do
        it 'raises an error' do
          expect { user.save }.not_to change{ User.count }

          expect(user.errors.first.full_message).to eq('This email address does not belong to an eligible TVE member')
        end
      end

      context 'when a related Eligible instance exists' do
        it 'creates an user' do
          create(:eligibility, email: email)

          expect { user.save }.to change{ User.count }.by(1)
        end

        context 'when the email address is provided with a different casing' do
          let(:email) { 'TesT@Example.com' }

          it 'creates an user' do
            create(:eligibility, email: email.downcase)

            expect { user.save }.to change{ User.count }.by(1)
          end
        end
      end
    end
  end
end
