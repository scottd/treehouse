# Treehouse 

This Rails application provides services used by members of the [Treehouse
Village Ecohousing](https://treehousevillage.ca) community.

## Development setup

If you wish to contribute to this project, you can set up your local
environment using `asdf`. Alternatively, feel free to skip the following and
install Ruby, Postgresql, NodeJS and Redis manually.

To set up your environment with `asdf`:

1. [Install asdf](https://asdf-vm.com/guide/getting-started.html) locally.
1. Make sure you install [dependencies needed for the asdf Postgres plugin](https://github.com/smashedtoatoms/asdf-postgres#dependencies).
   - If you're a Debian-based distro user, you may also want to add the
     following package: `libpq-dev`.
1. Install all `asdf` plugins needed for this project, get all needed gems and setup
   your local database by running the following:
   ```
   bin/dev_setup
   ```

Start your server by running:
```
foreman start
```
and the app should be available at
[http://localhost:3000](http://localhost:3000).

## Tests

To run all tests:

```
bundle exec rspec
```
