namespace :data_maintenance do
  desc 'Imports email addresses of all eligible users'
  task import_eligibilities: :environment do
    filepath = ARGV[1]

    if filepath.blank?
      puts 'Error - Please provide a input file as argument, e.g:'
      puts '$ rake data_maintenance:import_eligibilities /path/to/file'
      exit
    end

    unless File.file?(filepath)
      puts "Error - '#{filepath}' does not exist."
      exit
    end

    File.foreach(filepath, chomp: true) do |email|
      Eligibility.create(email: email.downcase)
    end

    puts "Done. We now have a total of #{Eligibility.all.count} eligible users."
  end
end
