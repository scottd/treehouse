class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  validates_presence_of :email, :first_name, :last_name
  validate :has_eligibility, on: :create

  def full_name
    "#{first_name} #{last_name}"
  end

  private

  def has_eligibility
    return if email && Eligibility.find_by(email: email.downcase).present?

    errors.add(:base, 'This email address does not belong to an eligible TVE member')
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end
end
